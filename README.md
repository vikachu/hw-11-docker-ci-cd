# Docker run

## Docker compose
`docker-compose up`

## Docker compose local build
`docker-compose -f docker-compose-local.yml up --build`

## Docker pull client image
`docker pull vikachu/chat-react-client`

## Docker pull server image
`docker pull vikachu/chat-express-server`

## Docker run client app container separately
`docker run -it -p 3000:3000 --name chat-react-client vikachu/chat-react-client`

## Docker run server app container separately
`docker run -it -p 3001:3001 --name chat-express-server vikachu/chat-express-server`
  


# Run locally
### Client Dev run (in `/client`)
```npm run dev```

### Server Dev run (in `/server`)
```npm run dev```
  
  
# Heroku
[Click here](https://bsa-2020-react-redux-saga.herokuapp.com/login) to open heroku app.
