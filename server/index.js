import express from "express";
import cors from "cors";
import bodyParser from "body-parser";

import routes from "./routes/routes"

const app = express();

app.use(cors());
app.use(bodyParser.json());

routes(app);

app.listen(process.env.PORT || 3001, () => {
  console.log(`app is listening to port 3001`);
});
