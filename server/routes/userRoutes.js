import { Router } from "express";
import * as userService from "../services/userService";

const router = Router();

router.get("/", (req, res, next) => {
  const users = userService.getUsers();
  res.send(users);
});

router.get("/:id", (req, res, next) => {
  try {
    const user = userService.getUserById(req.params.id);
    res.send(user);
  } catch {
    next();
  }
});

router.post("/", (req, res, next) => {
  const user = userService.createUser(req.body);
  res.send(user);
});

router.put("/:id", (req, res, next) => {
  const user = userService.updateUser(req.params.id, req.body);
  res.send(user);
});

router.delete("/:id", (req, res, next) => {
  const user = userService.deleteUser(req.params.id);
  res.send(user);
});

export default router;
