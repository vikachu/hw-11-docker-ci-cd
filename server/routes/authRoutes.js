import { Router } from "express";
import * as userService from "../services/userService";

const router = Router();

router.post("/login", (req, res, next) => {
  const user = userService.getUserByEmail(req.body.email);
  if (req.body.password === user.password) {
    res.send(user);
  } else {
    res.status(401).send();
  }
});

export default router;
