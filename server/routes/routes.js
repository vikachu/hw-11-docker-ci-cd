import messageRoutes from './messageRoutes';
import userRoutes from './userRoutes';
import authRoutes from './authRoutes';

export default app => {
  app.use('/api/messages', messageRoutes);
  app.use('/api/users', userRoutes);
  app.use('/api/auth', authRoutes);
};