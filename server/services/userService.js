import UserRepository from "../repositories/userRepository";

export const getUsers = () => UserRepository.getAll();

export const getUserById = (id) =>
  UserRepository.getOne((user) => user.id === id);

export const getUserByEmail = (email) =>
  UserRepository.getOne((user) => user.email === email);

export const createUser = (user) => {
  UserRepository.create({ ...user, isAdmin: false });
};

export const updateUser = (id, data) => UserRepository.update(id, data);

export const deleteUser = (id) => UserRepository.delete(id);
