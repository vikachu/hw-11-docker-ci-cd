import { actionTypes } from "../actions/userActionTypes";

const initialState = {
  users: [],
  isLoaded: false,
  isAdmin: null,
  currentUser: {}
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_USERS: {
      return { ...state, isLoaded: false };
    }

    case actionTypes.FETCH_USERS_SUCCESS: {
      const { users } = action.payload;
      return {
        ...state,
        users: users,
        isLoaded: true,
      };
    }

    case actionTypes.LOGIN_USER_SUCCESS: {
      const user = action.payload;
      return {
        ...state,
        isAdmin: user.isAdmin,
        currentUser: user
      };
    }

    default:
      return state;
  }
}
