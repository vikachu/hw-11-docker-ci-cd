import { combineReducers } from "redux";
import messagesReducer from "./messagesReducer";
import modalReducer from "./modalReducer";
import usersReducer from "./usersReducer";
import userFormReducer from "./userFormReducer";

const rootReducer = combineReducers({
  messagesReducer,
  modalReducer,
  usersReducer,
  userFormReducer,
});

export default rootReducer;
