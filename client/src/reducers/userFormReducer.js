import { actionTypes } from "../actions/userFormActionTypes";

const initialState = {
  user: {
    user: "",
    email: "",
    password: "",
    avatar: "",
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_USER_SUCCESS: {
      const { user } = action.payload;
      return {
        ...state,
        user: user,
      };
    }

    default:
      return state;
  }
}
