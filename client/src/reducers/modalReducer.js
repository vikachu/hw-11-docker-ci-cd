import { actionTypes } from "../actions/modalActionTypes";

const initialState = {
  editingMessage: {
    id: "",
    text: "",
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_MESSAGE_SUCCESS: {
      const { message } = action.payload;
      return {
        ...state,
        editingMessage: {
          id: message.id,
          text: message.text,
        },
      };
    }

    default:
      return state;
  }
}
