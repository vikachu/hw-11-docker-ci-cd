import moment from "moment";
import { actionTypes } from "../actions/messageActionTypes";

const initialState = {
  messages: [],
  isLoaded: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_MESSAGES: {
      return { ...state, isLoaded: false };
    }

    case actionTypes.FETCH_MESSAGES_SUCCESS: {
      const { messages } = action.payload;
      messages.sort((a, b) => moment(a.createdAt).diff(b.createdAt));
      return {
        ...state,
        messages: messages,
        isLoaded: true,
      };
    }

    default:
      return state;
  }
}
