import axios from "axios";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { actionTypes } from "../actions/userFormActionTypes";
import { API_URL } from "../config";

export function* fetchUser(action) {
  try {
    const id = action.payload.id;
    const user = yield call(axios.get, `${API_URL}/users/${id}`);

    yield put({ type: "FETCH_USER_SUCCESS", payload: { user: user.data } });
  } catch (error) {
    alert(`fetchUsererror: ${error.message}`);
  }
}

function* watchFetchUser() {
  yield takeEvery(actionTypes.FETCH_USER, fetchUser);
}

export default function* userFormSagas() {
    yield all([
        watchFetchUser(),
    ]);
  }