import axios from "axios";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { actionTypes } from "../actions/modalActionTypes";
import { API_URL } from "../config";

export function* fetchMessage(action) {
  try {
    const id = action.payload.id;
    const message = yield call(axios.get, `${API_URL}/messages/${id}`);

    yield put({ type: "FETCH_MESSAGE_SUCCESS", payload: { message: message.data } });
  } catch (error) {
    alert(`fetchMessages error: ${error.message}`);
  }
}

function* watchFetchMessage() {
  yield takeEvery(actionTypes.FETCH_MESSAGE, fetchMessage);
}

export default function* modalSagas() {
    yield all([
      watchFetchMessage(),
    ]);
  }