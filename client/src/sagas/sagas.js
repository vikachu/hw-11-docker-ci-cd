import { all } from "redux-saga/effects";
import messagesSagas from "./messagesSagas";
import modalSagas from "./modalSagas";
import usersSagas from "./usersSagas";
import userFormSagas from "./userFormSagas";

export default function* rootSaga() {
  yield all([messagesSagas(), modalSagas(), usersSagas(), userFormSagas()]);
}
