import axios from "axios";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { actionTypes } from "../actions/userActionTypes";
import { API_URL } from "../config";

export function* fetchUsers() {
  try {
    const response = yield call(axios.get, `${API_URL}/users`);
    const users = response.data;

    yield put({ type: "FETCH_USERS_SUCCESS", payload: { users } });
  } catch (error) {
    alert(`fetchUsers error: ${error.message}`);
  }
}

function* watchFetchUsers() {
  yield takeEvery(actionTypes.FETCH_USERS, fetchUsers);
}

export function* addUser(action) {
  const newUser = action.payload.data;

  try {
    yield call(axios.post, `${API_URL}/users`, newUser);
    yield put({ type: "FETCH_USERS" });
  } catch (error) {
    alert(`addUser error: ${error.message}`);
  }
}

function* watchAddUser() {
  yield takeEvery(actionTypes.ADD_USER, addUser);
}

export function* editUser(action) {
  const id = action.payload.id;
  const user = action.payload.data;

  try {
    yield call(axios.put, `${API_URL}/users/${id}`, user);
    yield put({ type: "FETCH_USERS" });
  } catch (error) {
    alert(`editUser error: ${error.message}`);
  }
}

function* watchEditUser() {
  yield takeEvery(actionTypes.EDIT_USER, editUser);
}

export function* deleteUser(action) {
  const id = action.payload.id;

  try {
    yield call(axios.delete, `${API_URL}/users/${id}`);
    yield put({ type: "FETCH_USERS" });
  } catch (error) {
    alert(`deleteUser error: ${error.message}`);
  }
}

function* watchDeleteMessage() {
  yield takeEvery(actionTypes.DELETE_USER, deleteUser);
}

export function* loginUser(action) {
  try {
    const user = yield call(axios.post, `${API_URL}/auth/login`, action.payload);
    yield put({ type: "LOGIN_USER_SUCCESS", payload: user.data });
  } catch (error) {
    alert(`loginUser error: ${error.message}`);
  }
}

function* watchLoginMessage() {
  yield takeEvery(actionTypes.LOGIN_USER, loginUser);
}

export default function* usersSagas() {
  yield all([
    watchFetchUsers(),
    watchAddUser(),
    watchEditUser(),
    watchDeleteMessage(),
    watchLoginMessage(),
  ]);
}
