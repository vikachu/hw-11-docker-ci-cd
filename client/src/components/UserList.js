import React from "react";
import { connect } from "react-redux";
import * as actions from "../actions/userActions";

import "../styles/UserList.scss";
import User from "./User";
import LoadingSpinner from "./LoadingSpinner";

class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onAddUserClick = this.onAddUserClick.bind(this);
    this.onEditUserClick = this.onEditUserClick.bind(this);
    this.onDeleteUserClick = this.onDeleteUserClick.bind(this);
    this.onChatClick = this.onChatClick.bind(this);
  }

  componentDidMount() {
    this.props.fetchUsers();
  }

  onAddUserClick() {
    this.props.history.push("/users/add");
  }

  onEditUserClick(userId) {
    this.props.history.push(`/users/${userId}`);
  }

  onDeleteUserClick(userId) {
    this.props.deleteUser(userId);
  }

  // TODO: navigation menu
  onChatClick() {
    this.props.history.push("/messages/");
  }

  render() {
    if (!this.props.isLoaded) {
      return <LoadingSpinner />;
    } else {
      return (
        <div className="users__container">
          <button className="users__button-chat" onClick={this.onChatClick}> Go to chat </button>
          <button className="users__button-add" onClick={this.onAddUserClick}>
            Add user
          </button>
          {this.props.users.map((user) => {
            return (
              <User
                key={user.id}
                id={user.id}
                name={user.user}
                email={user.email}
                avatar={user.avatar}
                onEditUserClick={this.onEditUserClick}
                onDeleteUserClick={this.onDeleteUserClick}
              />
            );
          })}
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.usersReducer.users,
    isLoaded: state.usersReducer.isLoaded,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
