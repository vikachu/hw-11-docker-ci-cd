import React from "react";
import { connect } from "react-redux";

import "../styles/UserForm.scss";
import * as actions from "../actions/userFormActions";
import { addUser, editUser } from "../actions/userActions";

// TODO: move to constant
const defaultUserConfig = {
  nameInput: "",
  emailInput: "",
  passwordInput: "",
  avatarInput: "",
};

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      email: "",
      password: "",
      avatar: "",
    };

    this.onChangeData = this.onChangeData.bind(this);
    this.onCancelButtonClick = this.onCancelButtonClick.bind(this);
    this.onSaveButtonClick = this.onSaveButtonClick.bind(this);
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.fetchUser(this.props.match.params.id);
    }
  }

  // TODO: change to getDerivedStateFromProps
  componentWillReceiveProps(nextProps) {
    this.setState({
      ...nextProps.user,
    });
  }

  onChangeData(e, keyword) {
    const value = e.target.value;
    this.setState({
      ...this.state,
      [keyword]: value,
    });
  }

  onCancelButtonClick() {
    this.props.history.push("/users");

    this.setState(defaultUserConfig);
  }

  onSaveButtonClick() {
    if (this.state.id) {
      this.props.editUser(this.state.id, this.state);
    } else {
      this.props.addUser(this.state);
    }

    this.props.history.push("/users");
    this.setState(defaultUserConfig);
  }

  render() {
    return (
      <div className="modal__layer">
        <div className="modal__root">
          <div className="modal__header">Add user</div>
          <div className="modal__body">
            <div className="user-inputs__container">
              <div className="user-field">
                <label className="user-field__label">Name</label>
                <input
                  className="user-field__input"
                  placeholder="name"
                  value={this.state.user}
                  type=""
                  onChange={(e) => this.onChangeData(e, "user")}
                />
              </div>
              <div className="user-field">
                <label className="user-field__label">Email</label>
                <input
                  className="user-field__input"
                  placeholder="email"
                  value={this.state.email}
                  type=""
                  onChange={(e) => this.onChangeData(e, "email")}
                />
              </div>
              <div className="user-field">
                <label className="user-field__label">Password</label>
                <input
                  className="user-field__input"
                  placeholder="password"
                  value={this.state.password}
                  type=""
                  onChange={(e) => this.onChangeData(e, "password")}
                />
              </div>
              <div className="user-field">
                <label className="user-field__label">Avatar</label>
                <input
                  className="user-field__input"
                  placeholder="link"
                  value={this.state.avatar}
                  type=""
                  onChange={(e) => this.onChangeData(e, "avatar")}
                />
              </div>
            </div>
          </div>
          <div className="modal__footer">
            <button
              className="modal_button modal_button-cancel"
              onClick={this.onCancelButtonClick}
            >
              Cancel
            </button>

            <button
              className="modal_button modal_button-save"
              onClick={this.onSaveButtonClick}
            >
              Save
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userFormReducer.user,
  };
};

const mapDispatchToProps = {
  ...actions,
  addUser,
  editUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);
