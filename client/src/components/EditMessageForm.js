import React from "react";
import { connect } from "react-redux";
import "../styles/EditMessageForm.scss";
import * as actions from "../actions/modalActions";
import { editMessage } from "../actions/messageActions";

class EditMessageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editMessageInput: "",
    };

    this.onEditMessageInputChange = this.onEditMessageInputChange.bind(this);
    this.onCancelButtonClick = this.onCancelButtonClick.bind(this);
    this.onSaveButtonClick = this.onSaveButtonClick.bind(this);
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.fetchMessage(this.props.match.params.id);
    }
  }

  // TODO: change to getDerivedStateFromProps
  componentWillReceiveProps(nextProps) {
    this.setState({
      ...this.state,
      editMessageInput: nextProps.editingMessage.text,
    });
  }

  async onEditMessageInputChange(e) {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      editMessageInput: value,
    });
  }

  onCancelButtonClick() {
    this.props.history.push("/messages");

    this.setState({
      ...this.state,
      editMessageInput: "",
    });
  }

  onSaveButtonClick() {
    this.props.editMessage(this.props.editingMessage.id, {
      text: this.state.editMessageInput,
    });

    this.props.history.push("/messages");
    this.setState({
      ...this.state,
      editMessageInput: "",
    });
  }

  render() {
    return (
      <div className="modal__layer">
        <div className="modal__root">
          <div className="modal__header">Edit message</div>
          <div className="modal__body">
            <textarea
              className="modal_text"
              value={this.state.editMessageInput}
              onChange={this.onEditMessageInputChange}
            ></textarea>
          </div>
          <div className="modal__footer">
            <button
              className="modal_button modal_button-cancel"
              onClick={this.onCancelButtonClick}
            >
              Cancel
            </button>

            <button
              className="modal_button modal_button-save"
              onClick={this.onSaveButtonClick}
            >
              Save
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    editingMessage: state.modalReducer.editingMessage,
  };
};

const mapDispatchToProps = {
  ...actions,
  editMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageForm);
