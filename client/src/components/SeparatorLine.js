import React from "react";
import PropTypes from "prop-types";
import "../styles/SeparatorLine.scss";

const SeparatorLine = ({ separatorDate }) => {
  return (
    <div className="separator-line__container">
      <div className="separator-line__line">
        <div className="separator-line__text">{separatorDate}</div>
      </div>
    </div>
  );
};

SeparatorLine.propTypes = {
  separatorDate: PropTypes.string,
};

export default SeparatorLine;
