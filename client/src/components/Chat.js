import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import * as actions from "../actions/messageActions";
import "../styles/Chat.scss";

import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import LoadingSpinner from "./LoadingSpinner";
import Logo from "./Logo";
import Copyright from "./Copyright";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editInput: "", // TODO: remove
      input: "",
    };
    this.onMessageInputChange = this.onMessageInputChange.bind(this);
    this.onLikeClick = this.onLikeClick.bind(this);
    this.onDeleteMessageClick = this.onDeleteMessageClick.bind(this);
    this.onEditMessageClick = this.onEditMessageClick.bind(this);

    this.onSendMessageClick = this.onSendMessageClick.bind(this);
  }

  componentDidMount() {
    this.props.fetchMessages();
  }

  async onMessageInputChange(e) {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      input: value,
    });
  }

  onSendMessageClick() {
    if (this.state.input !== "") {
      const message = {
        text: this.state.input,
        user: this.props.currentUser.name,
        avatar: this.props.currentUser.avatar,
        userId: this.props.currentUser.id,
      };
      this.props.sendMessage(message);

      this.setState({
        ...this.state,
        input: "",
      });
    }
  }

  onLikeClick(clickedMessage) {
    if (this.props.currentUser.id !== clickedMessage.userId) {
      this.props.likeMessage(clickedMessage.id);
    }
  }

  onDeleteMessageClick(clickedMessage) {
    if (this.props.currentUser.id === clickedMessage.userId) {
      this.props.deleteMessage(clickedMessage.id);
    }
  }

  onEditMessageClick(clickedMessage) {
    if (this.props.currentUser.id === clickedMessage.userId) {
      this.props.history.push(`/messages/${clickedMessage.id}`);
    }
  }

  render() {
    if (!this.props.isLoaded) {
      return <LoadingSpinner />;
    } else {
      const messages = this.props.messages;
      const userIds = new Set();
      messages.map((message) => userIds.add(message.userId));

      const lastMessageTime = moment(
        messages[messages.length - 1].createdAt
      ).format("hh:mm");

      return (
        <div>
          <div className="chat__container">
            <Logo />
            <Header
              messagesCount={messages.length}
              participantsCount={userIds.size}
              lastMessageTime={lastMessageTime}
            />
            <MessageList
              messages={this.props.messages}
              currentUserId={this.props.currentUser.id}
              onLikeClick={this.onLikeClick}
              onDeleteMessageClick={this.onDeleteMessageClick}
              onEditMessageClick={this.onEditMessageClick}
            />
            <MessageInput
              onMessageInputChange={this.onMessageInputChange}
              onSendMessageClick={this.onSendMessageClick}
              inputValue={this.state.input}
            />
            <Copyright />
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isLoaded: state.messagesReducer.isLoaded,
    messages: state.messagesReducer.messages,
    currentUser: state.usersReducer.currentUser,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
