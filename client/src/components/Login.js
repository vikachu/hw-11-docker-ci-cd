import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import * as actions from "../actions/userActions";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };

    this.onLoginButtonClick = this.onLoginButtonClick.bind(this);
  }

  onChangeData(e, keyword) {
    const value = e.target.value;
    this.setState({
      ...this.state,
      [keyword]: value,
    });
  }

  onLoginButtonClick() {
    this.props.loginUser(this.state.email, this.state.password);
  }

  render() {
    if (this.props.isAdmin === true) {
      return <Redirect to="/users" />;

    } else if (this.props.isAdmin === false) {
      return <Redirect to="/messages" />;

    } else {
      return (
        <div className="modal__layer">
          <div className="modal__root">
            <div className="modal__body">
              <div className="user-field">
                <label className="user-field__label">Email</label>
                <input
                  className="user-field__input"
                  placeholder="email"
                  value={this.state.email}
                  type=""
                  onChange={(e) => this.onChangeData(e, "email")}
                />
              </div>
              <div className="user-field">
                <label className="user-field__label">Password</label>
                <input
                  className="user-field__input"
                  placeholder="password"
                  value={this.state.password}
                  type=""
                  onChange={(e) => this.onChangeData(e, "password")}
                />
              </div>

              <button className="" onClick={this.onLoginButtonClick}>
                Login
              </button>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isAdmin: state.usersReducer.isAdmin,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
