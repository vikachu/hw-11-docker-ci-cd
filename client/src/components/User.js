import React from "react";

import "../styles/User.scss";

class User extends React.Component {
  render() {
    return (
      <div className="user__container">
        <div className="user__avatar">
          <img src={this.props.avatar} alt="avatar"></img>
        </div>
        <div className="user__name">{this.props.name}</div>
        <div className="user__email">{this.props.email}</div>
        <div className="user__button-block">
          <button
            className="user__button-edit"
            onClick={() => this.props.onEditUserClick(this.props.id)}
          >
            Edit
          </button>

          <button
            className="user__button-delete"
            onClick={() => this.props.onDeleteUserClick(this.props.id)}
          >
            Delete
          </button>
        </div>
      </div>
    );
  }
}

export default User;
